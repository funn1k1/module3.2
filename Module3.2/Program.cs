using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module3_2
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
            //Task1
            Task4 A = new Task4();
            int[] arr = A.GetFibonacciSequence(25);
            foreach (int number in arr)
            {
                Console.Write(number + "|");
            }
            Console.WriteLine();
            //Task2
            Task5 B = new Task5();
            int num = B.ReverseNumber(-973206445);
            Console.WriteLine(num);
            //Task3
            Task6 C = new Task6();
            int[] generateArr = C.GenerateArray(10);
            Console.WriteLine("Исходный массив:");
            foreach (int number in generateArr)
            {
                Console.Write(number + "|");
            }
            Console.WriteLine();
            Console.WriteLine("Результат:");
            generateArr = C.UpdateElementToOppositeSign(generateArr);
            foreach (int number in generateArr)
            {
                Console.Write(number + "|");
            }
            Console.WriteLine();
            //Task4
            Task7 D = new Task7();
            generateArr = D.GenerateArray(10);
            Console.WriteLine("Исходный массив:");
            foreach (int number in generateArr)
            {
                Console.Write(number + "|");
            }
            Console.WriteLine();
            Console.WriteLine("Результат:");
            List<int> vs = D.FindElementGreaterThenPrevious(generateArr);
            foreach (int number in vs)
            {
                Console.Write(number + "|");
            }
            Console.WriteLine();
            //Task5
            Task8 F = new Task8();
            int[,] myarr = F.FillArrayInSpiral(4);
            Console.WriteLine("Результат заполнения спиралью:");
            for (int i = 0; i < myarr.GetLength(0); i++)
            {
                for (int j = 0; j < myarr.GetLength(1); j++)
                {
                    Console.Write($"{myarr[i, j]}\t");
                }
                Console.WriteLine();
            }

        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if (Int32.TryParse(input, out result) && result >= 0) return true;
            else return false;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int j = 0;
            int[] arr = new int[n];
            for (int i = 1, m = 0; m < n; i += j, m++)
            {
                arr[m] = j;
                j = i - j;
            }
            return arr;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            string str = Math.Abs(sourceNumber).ToString();
            char[] symb = str.ToCharArray();
            Array.Reverse(symb);
            if (sourceNumber < 0) return -Int32.Parse(new string(symb));
            else return Int32.Parse(new string(symb));
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] arr ;
            if (size <= 0)
            {
                arr = new int[0];
                return arr;
            }
            arr = new int[size];
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(-100, 100);
            }
            return arr;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                    source[i] = -source[i];
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] arr;
            if (size <= 0)
            {
                arr = new int[0];
                return arr;
            }
            arr = new int[size];
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(0, 100);
            }
            return arr;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> vs = new List<int>();
            for (int i = 0; i < source.Length - 1; i++)
            {
                if (source[i] < source[i + 1])
                {
                    vs.Add(source[i + 1]);
                }
            }
            return vs;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] arr = new int[size, size];
            int value = 1, row1 = 0, col1 = 0, row2 = size - 1, col2 = size - 1, i;
            while (value <= size * size)
            {
                for (i = col1; i <= col2; i++)
                    arr[row1, i] = value++;
                for (i = row1 + 1; i <= row2; i++)
                    arr[i, col2] = value++;
                for (i = col2 - 1; i >= col1; i--)
                    arr[row2, i] = value++;
                for (i = row2 - 1; i >= row1 + 1; i--)
                    arr[i, col1] = value++;
                col1++;
                col2--;
                row1++;
                row2--;
            }
            return arr;
        }
    }
}